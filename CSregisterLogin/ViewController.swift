//
//  ViewController.swift
//  CSregisterLogin
//
//  Created by user on 05/02/18.
//  Copyright © 2018 user. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var userPassword: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var genderPicker: UIPickerView!
    
    var pickerData = [String]()
    var userGender = String()
    var userDob = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userName.delegate = self
        userName.tag = 0
        userName.returnKeyType = UIReturnKeyType.next
        
        userEmail.delegate = self
        userEmail.tag = 1
        userEmail.returnKeyType = UIReturnKeyType.next
        
        userPassword.delegate = self
        userPassword.tag = 2
        userPassword.returnKeyType = UIReturnKeyType.done
        
        //connect data
        self.genderPicker.delegate = self
        self.genderPicker.dataSource = self
        
        //input data
        pickerData = ["Male", "Female", "Other"]
        userGender = pickerData[0]
       
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        userGender = pickerData[row]
        print("Gender" + userGender)
    }
    
    
    @IBAction func datePickerChanged(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, YYYY"
        userDob = dateFormatter.string(from: sender.date)
        
    }
    
    
    func showError(title: String, message: String){
        let alertcontroller = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertcontroller.addAction(UIAlertAction(title:"Ok", style:UIAlertActionStyle.default, handler: nil ))
        present(alertcontroller, animated: true, completion: nil)
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func isValidPassword(testStr:String) -> Bool{
        let passwordRegex = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()-_=+{}|?>.<,:;~`’]{8,}$"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: testStr)
    }
    
    
    @IBAction func registerNow(_ sender: Any) {
        let name = userName.text ?? ""
        let email = userEmail.text ?? ""
        let password = userPassword.text ?? ""
        
        
        if name == "" || email == "" || password == ""{
            showError(title: "Sorry !", message: "Fields Cannot be Empty")
        }
        else{
            let check_mail = isValidEmail(testStr: email)
            let check_pwd = isValidPassword(testStr: password)
            
            if check_mail && check_pwd == true{
                print("validated")
                
                UserDefaults.standard.set(name, forKey: "name")
                UserDefaults.standard.set(email, forKey: "email")
                UserDefaults.standard.set(password, forKey: "password")
                UserDefaults.standard.set(userGender, forKey: "gender")
                self.performSegue(withIdentifier: "gotologin", sender: self)
            }
            else{
                showError(title: "Sorry !", message: "Email/Password requirements not satisfied")
                
            }
            print(name, email, password)
            print(userDob)
            print(userGender)
            
            
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            return true;
        }
        return false

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

